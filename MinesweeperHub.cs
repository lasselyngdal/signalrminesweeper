﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace SignalRMinesweeper
{
    public class MinesweeperHub : Hub
    {
        private const int Columns = 20;
        private const int Rows = 20;

        private static GameState currentState = GameState.Lobby;

        // Create static properties in order to share data between hubs.
        private static int[] Grid = new int[Rows * Columns];
        // Key<who, am I dead>
        private static readonly PlayerRegister Players = new PlayerRegister();
        private static readonly PlayerRegister DeadPlayers = new PlayerRegister();

        public async Task SendMessage(int x, int y)
        {
            string user = Context.ConnectionId;

            if (Grid[GetIndex(x, y)] == 1)
            {
                DeadPlayers.Add(user);
                Debug.WriteLine($"Dead: {DeadPlayers.ToListString()}");
            }

            if (!DeadPlayers.Contains(user))
            {
                Grid[GetIndex(x, y)] = 1;
                await Clients.Others.SendAsync("ReceiveMessage", x, y);
            }

            await Clients.Caller.SendAsync("DidIDie", DeadPlayers.Contains(user));
        }

        public override Task OnConnectedAsync()
        {
            Clients.Caller.SendAsync("GetGrid", Grid, Columns, Rows);

            Players.Add(Context.ConnectionId);

            foreach (string player in Players)
                Debug.WriteLine($"Connected: {player}");

            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            Players.Remove(Context.ConnectionId);

            return base.OnDisconnectedAsync(exception);
        }

        private int GetIndex(int x, int y)
        {
            return (y * Rows) + x;
        }
    }

    public enum GameState { Lobby = 1, Game = 2 }
}
