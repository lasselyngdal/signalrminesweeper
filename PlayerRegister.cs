﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Migrations.Operations;

namespace SignalRMinesweeper
{
    public class PlayerRegister
    {
        private readonly HashSet<string> Register = new HashSet<string>();

        public void Add(string connectionId)
        {
            if (connectionId == null)
                return;

            // Thread safety 
            lock (Register)
                Register.Add(connectionId);
        }

        public IEnumerator<string> GetEnumerator()
        {
            return Register.GetEnumerator();
        }

        public string ToListString()
        {
            var builder = new StringBuilder();
            foreach (string val in Register)
            {
                builder.AppendLine(val);
            }

            return builder.ToString();
        }

        public bool Contains(string connectionId)
        {
            if (connectionId == null)
                return false;

            lock (Register)
                return Register.Contains(connectionId);
        }

        public void Remove(string connectionId)
        {
            if (connectionId == null)
                return;

            lock (Register)
                Register.Remove(connectionId);
        }

    }
}
