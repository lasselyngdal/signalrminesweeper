SignalRMinesweeper
==================
# About
A little game showcasing a simple use case for using SignalR. 
A game similar to minesweeper is contained in this repository.

## Game rules
Each player has the objective of getting as many points as possible. There's a catch though. If the field has been taken by another player, the player choosing the depleted field will die.
For each field the player gathers add +N points to their score.
If the player gets the final field +N points is added to their score

# Status
Early stages, nothing fancy has been setup, just the bare minimums.

# Visual Studio setup
Import/open the project using Visual Studio.
For developers be advised this uses libman. To restore the packages required by the front-end side of things please refer to https://docs.microsoft.com/en-us/aspnet/core/client-side/libman/libman-cli?view=aspnetcore-2.2 and use the command ```libman restore``` in the project.
The rest should be handled by the built in NuGet package manager.
When everything has been restored you free to open up the game.
