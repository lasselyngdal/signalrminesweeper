﻿"use strict";

const WIDTH = 800;
const HEIGHT = 600;

var gameVars = {
    gameOver: false,
    columns: 0,
    rows: 0,
    boxWidth: 0,
    boxHeight: 0,
    grid: 0
};

var lobbyVars = {
};

const GameState = {
    LOBBY: 1,
    GAME: 2
};

var currentState = GameState.GAME;

// SignalR config
//const connection = new signalR.HubConnectionBuilder().withUrl("index.html").build();
var connection = new signalR.HubConnectionBuilder()
    .configureLogging(signalR.LogLevel.Trace)
    .withUrl("minesweeper")
    .build();

// Run on setup
function setup() {
    // P5
    // createCanvas(WIDTH, HEIGHT);
    frameRate(15);
    textAlign(CENTER, CENTER);

    // SIGNALR
    connection.start().then(() => {
        console.log("Connected to the hub");

        connection.on("GetGrid", (grid, columns, rows) => {
            console.log("GetGrid");

            gameVars.columns = columns;
            gameVars.rows = rows;

            gameVars.boxWidth = WIDTH / gameVars.columns;
            gameVars.boxHeight = HEIGHT / gameVars.rows;

            gameVars.grid = grid;
        });

		connection.on("GetGameState", (gameState) => {
			currentState = gameState;

			switch(gameState) {
				case GameState.GAME: 
					createCanvas(WIDTH, HEIGHT);
					break;
			}
		});

        connection.on("ReceiveMessage", (x, y) => {
            console.log("Received message grid updated on " + x + " " + y);
            gameVars.grid[getIndex(x, y)] = 1;
        });

        connection.on("DidIDie",
            (amIDead) => {
                gameVars.gameOver = amIDead;
                console.log(amIDead ? "I'm dead" : "I'm still alive");
            }
        );
    });

}

function getIndex(x, y) {
    return (y * gameVars.rows) + x;
}

// Tick every n
function draw() {
    background(240);

    switch (currentState) {
        case GameState.LOBBY:
			drawLobby();
            break;
        case GameState.GAME:
            drawGrid();
            break;
    }
}

function drawGrid() {
    for (let y = 0; y < gameVars.rows; ++y) {
        for (let x = 0; x < gameVars.columns; ++x) {
            if (gameVars.grid[getIndex(x, y)] == 1)
                fill(0);
            else
                fill(255);

            rect(x * gameVars.boxWidth
                , y * gameVars.boxHeight
                , gameVars.boxWidth - 1
                , gameVars.boxHeight - 1);
        }
    }

    if (gameVars.gameOver) {
		filter(BLUR, 3);
        fill(220);
		textSize(60);
        text("Game over m8", WIDTH / 2, HEIGHT / 2);
    }
}

function mousePressed() {
    for (let y = 0; y < gameVars.rows; ++y) {
        for (let x = 0; x < gameVars.columns; ++x) {
            if (withinField(x, y)) {
                if (!gameVars.gameOver) {
                    gameVars.grid[getIndex(x, y)] = 1;
                    connection.invoke("SendMessage", x, y)
                        .then(() => console.log("invoked SendMessage with grid " + x + " " + y))
                        .catch((err) => console.log(err.toString()));
                }
            }
        }
    }
}

function withinField(x, y) {
    return mouseX > x * gameVars.boxWidth &&
        mouseX < (x + 1) * gameVars.boxWidth &&
        mouseY > y * gameVars.boxHeight &&
        mouseY < (y + 1) * gameVars.boxHeight;
}
